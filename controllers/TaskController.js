const Task = require('../models/Task.js')

module.exports.createTask = (data) => {
    let newTask = new Task({
        name: data.name
    })
    
    return newTask.save().then((savedTask, error) => {
        if(error){
            console.log(error)
            return error
        }
        
        return savedTask
    })
}

module.exports.getAllTasks = () => {
    return Task.find({}).then((result) => {
        return result
    })
}

module.exports.updateTask = (task_id, new_data) => {
    return Task.findById(task_id).then((result, error) => {
        if(error){
            console.log(error)
            return error
        }

        result.name = new_data.name

        return result.save().then((updatedTask, error) => {
            if(error){
                console.log(error)
                return error
            }

            return updatedTask
        })
    })
}

// Mini Activity
module.exports.deleteTask = (task_id) => {
    return Task.findByIdAndDelete(task_id).then((deletedTask, error) => {
        if(error){
            console.log(error)
            return error
        }
        
        return deletedTask
    })
}

/*
module.exports.deleteTask = (task_id) => {
    return Task.findByIdAndDelete(task_id).then((deletedTask) => {
        // Checks if the id does not exist
        if(!deletedTask){
            return `Task not found`
        }
        
        return deletedTask
    })
}

*/

// 2. Create a controller function for retrieving a specific task.
module.exports.specificTask = (task_id) => {
    return Task.findById(task_id).then((specificId) => {
        if(!specificId){
            return `Task not found`
        }

        return specificId
    })
}

// 6. Create a controller function for changing the status of a task to complete.
module.exports.changeStatus = (task_id) => {
    return Task.findById(task_id).then((task) => {
        if(!task){
            return `Task not found`
        }
        
        task.status = "complete"

        return task.save().then((updatedStatus, error) => {
            if(error){
                console.log(error)
                return error
            }
            return updatedStatus
        })
    })
}